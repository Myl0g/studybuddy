//
//  MessagesViewController.swift
//  StudyBuddy
//
//  Created by Milo Gilad on 10/5/18.
//  Copyright © 2018 NBPS. All rights reserved.
//

import UIKit

class MessagesViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBOutlet weak var message: UITextField!
    @IBOutlet weak var messageDisplay: UILabel!
    
    @IBAction func sendMessage(_ sender: Any) {
        messageDisplay.text = message.text;
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
